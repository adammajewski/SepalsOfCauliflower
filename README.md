Local discrete complex dynamics near parabolic fixed point.  
Sepals of Leau-Fatou flower




# TOC
* Quadratic polynomial
  * [Period 1 = cauliflower Julia set ](#period-1)
  * [Period 2 = fat basilica Julia set](#period-2-basilica-julia-set)
  * period 3 = fat Douady rabbit
* [Cubic polynomial](#cubic-polynomial)

 
# Definitions ( key words)
* Leau-Fatou flower theorem
* [petal](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/parabolic#Petal) of Leau-Fatou flower
  * attracting petal
* [sepal ](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/parabolic#Sepal)
* [Discrete local parabolic dynamics](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/parabolic)
* map = complex quadratic polynomial 
  * [wikipedia](https://en.wikipedia.org/wiki/Complex_quadratic_polynomial)
  * [wikibooks](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/qpolynomials)
* Numerical aproximation of local dynamics (orbits near parabolic fixed point )
* Related algorithms 
  * [boolean escape time](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Julia_set#Boolean_Escape_time)
  * [parbolic chessboard](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/pcheckerboard)
  * [edge detection](https://en.wikibooks.org/wiki/Fractals/Computer_graphic_techniques/2D#Edge_detection)


# Period 1 


## New algorithm (distance)

![./images/c.png](./images/c.png)   

It is made with [c.c](./src/c.c)  

Steps
* take point z0 from dynamic plane
* make some forward iterations ( zn)
  * if zn escapes then z0 is from exterior ( stop)
  * if zn falls into the attracting petal then z0  is from interior ( go to the next step)
* take zn and make some backward ( inverse iteration ) checking for each iteration distance to the parabolic periodic  point. (Note that inverse iteration stay inside the same mian chessboard box)
* compare distance
  * if distance form next ( inverse)  iteration is bigger then distance from previous inverse iteration go to the next iteration
  * If distance is smaller stop = it is the biggest distance of parabolic orbit
* used biggest distance of orbit to colur all points from such orbit ( In parabolic case orbits do not cross  )

  
  

Compare with:
[Julia set by gerrit](https://fractalforums.org/image-threads/25/gerrit-images/565/new#new) - atom domains

> Julia set of z^2 + 1/4. Coloring is $`min_n|z_n|`$. The bright spots are the locations of the tree of minibrots in an embedded Julia set in the cusp of a mini, going up by the period of the that mini for each tree level. How that comes about I don't know.

```c
// ultrafractal 
c = 1/4, n=im=0, N=10000, zm=100
z = "pixel"
while(n++<N)
  z = z^2+c
  if |z|>2: break
  if |z|<zm: {zm=|z|,im=n}
endwhile
// color using either im ("atom domain") or zm as representation function


```


![./images/uf152.png](./images/uf152.png)   

  

## Old algorithm


![./images/1006.png](./images/1006.png)   


It is made with [d.c](./src/d.c)  


Steps: 


[Bailout test](#bailout-test-from-boolean-escape-time-algorithm) uses forward iteration for dividing points of dynamic plane into 2 groups:
* escaping ( quickly escaping points from exterior  )
* non-escaping ( mainly interior points with some boundary and slowly escaping points from exterior)

On the first image one can see 2 sets:
* escaping 
* non-escaping 

![./images/1000.png](./images/1000.png)   


Second image shows [parabolic chessboard](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/pcheckerboard).  
Forward orbits of all interior points tend to fixed point. Check the position of z near fixed point (after n iterations).   
If it's imaginary part is positive it is upper main box, if negative lower one. 

On the second image one can see 4 sets:
* escaping 
* non-escaping ( sum of below subsets)
  * upper main box and it's preimages
  * lower main box and it's preimages'

![./images/1001.png](./images/1001.png)   


Third image shows only boundaries of parabolic chessboard from above image

![./images/1002.png](./images/1002.png)   

Fourth image shows parabolic chessboard with boundaries


![./images/1003.png](./images/1003.png)   

Fifth image shows new algorithm showing dynamics inside parabolic Julia set. Here invariat curves are:
* perpendiculra to level curves of attracting time
* parallel to orbits

![1006.png](1006.png)   

 Here is  
 * [big version](d.png) 
 * [commons version](https://commons.wikimedia.org/wiki/File:Parabolic_sepals_for_internal_angle_1_over_1.png)

 Compare with this image with:
 * parabolic chessboard
 * level sets of attraction time
 
 It is inspired by [tiles by T Kawahira](http://www.math.titech.ac.jp/~kawahira/gallery/tiles/tiles.html)
 
![Interior_of_the_Cauliflower_Julia_set.png](./images/Interior_of_the_Cauliflower_Julia_set.png)

The source code is in [the commons](https://commons.wikimedia.org/wiki/File:Interior_of_the_Cauliflower_Julia_set.png).  

Next image is made with [program Mandel by Wolf Jung](http://www.mndynamics.com/indexp.html)

![cauliflower_sepals.png](./images/cauliflower_sepals.png)



It shows some forward orbits of points from interior of Julia set. These points:
* first escape from fixed point
* later ( after some iterations) tend to the fixed point  
Such orbits lie on the invariant curvs.

One can see that are 2 sepals near parabolic fixed point  
  

### Output

Program creates :
* 7 still 2D images in pgm format 
* text output 

### Steps of old algorithm 
* divide complex dynamic plane using escape time ( forward iteration) and bailout test into 2 subsets : 
 * escaping points = exterior = iColorOfExterior
 * non-escaping points = interior  = iColorOfInterior
* leave escaping points unchanged and take only nonescaping points, compute parabolic chessboard 
* compute boundaries of sets using Sobel filter 
* find 2 main chessboard boxes and color it using maximal distance from orbit to fixed point 
* create image

### Functions  
* ComputeFatouComponents -> ComputeColor ( [bailout test](#bailout-test-from-boolean-escape-time-algorithm) = esacping/non-escaping)
* ComputeZerosOfQnc (parabolic chessboard)
* ComputeBoundariesIn and CopyBoundariesTo ( Sobel filter for boundaries)
* FillContour2 ( color 2 main chessboard boxes)
* SaveArray2PGMFile




### To do

Remove kinks from image
use averade distance instead of maximal distance


# Period 2 ( Fat Basilica Julia set)
This image is made with [program Mandel by Wolf Jung](http://www.mndynamics.com/indexp.html)

![basilica_sepals.png](./images/basilica_sepals.png)

It shows some forward orbits of points from interior of Julia set. These points:
* first escape from fixed point
* later ( after some iterations) tend to the fixed point  
Such orbits lie on the invariant curvs,

This is my image, but I do not know how I have made it ( I do not save the code and could not redo it )  

![e.png](./images/e.png)

  

This is beautiful and also interesting from a mathematic point of view image by [Vladlen Timorin](https://www.mccme.ru/~timorin/) from [his slide](https://www.hse.ru/data/2014/06/24/1310210120/moscow-2014.pdf)  

He kindly let me use his graphic (png file) 

> As far as I remember, I didn’t do inverse iterations. But I checked the angles at which the forward orbit approaches the parabolic point. 



![moscow-2014-005.png](./images/moscow-2014-005.png)  

And here is thresholding transformation ( made with gimp)

![moscow-prog.png](./images/moscow-prog.png)  

compare with checkerboard ( [code and desciption](https://commons.wikimedia.org/wiki/File:A_parabolic_checkerboard_for_rotation_number_1_over_2.png))
![basilica_checkerboard.png](./images/basilica_checkerboard.png)


# Cubic polynomial
Example from paper : [Note on dynamically stable perturbations of parabolics](http://www.math.titech.ac.jp/~kawahira/works/rims0402.pdf) by Tomoki Kawahira 

> Set g(z) := z(1 − z2), with a parabolic at z = 0. The attracting (resp. repelling) directions lie on R (resp. iR). Let l1 be an invariant curve in the first quadrant and L1 the region enclosed by l1 ∪ {0}, called a sepal. 
> For i = 2, 3, and 4, let li and Li be the symmetric image of l1 and L1 in the i-th quadrant. (See Figure 7.) In particular, L1 and L3 (resp. L2 and L4) are called right sepals (resp. left sepals). 


This image is made with [program Mandel by Wolf Jung](http://www.mndynamics.com/indexp.html)

![cubic_sepals.png](./images/cubic_sepals.png)


It shows some forward orbits of points from interior of Julia set. These points:
* first escape from fixed point
* later ( after some iterations) tend to the fixed point  
Such orbits lie on the invariant curvs,





# Digital 2D raster image processing

## color
Here 8 bit color is used = [shades of gray](https://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_palettes#8-bit_Grayscale) = integer values from 0 to 255

## Raster 2D images 

Image exist in 3 modes :
* in the virtual mode ( = in the memory ) as a array of pixel colors
 * 1D memory array of unsigned char numbers 
 * 2D memory array of unsigned char numbers 
* on the disk as a pgm file 

Program uses 3 arrays : 

````c
// memmory 1D array 
unsigned char *data;
unsigned char *edge;
unsigned char *zero;
````

## Creating images ( from main function)

Flow of creating image :
* crete array in the memory : function malloc
* modify array
 * fill the whole rectangle 
 * find and copy boundaries : function ComputeBoundariesIn  and CopyBoundariesTo
* save array as a pgm file on the disk ( function SaveArray2PGMFile)
* free memory = delete array ( function free )




````c
setup(iPeriodParent, iPeriodChild); // create array
ComputeZerosOfQnc(zero, n); // fill array
SaveArray2PGMFile( zero, n+0.1); // save array
free(zero); // free memeory
````


## Creating one image = fill array
* scan virtual 2D array using integer coordinate : ( ix,iy)
* for each point (ix,iy) do : 
 * convert integer coordinate ( ix,iy) to the world ( real) coordinate :
 * convert ix,iy to index of virtual 1D array : i = Give_i(ix,iy); 
 * compute color of point : iColor = ComputeColor(ix, iy, IterationMax);
 * save color to the array :  A[i] = iColor;
 


 


# main loop

## simple tests

### bailout test ( boolean escape time algorithm )

Teste if points escapes ( = is attracted to infinity). Note that infinity is fixed point  

[Bailout test ](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Julia_set#Boolean_Escape_time)
check if magnitude ( absolute value) of z is grater then escape radius ( ER)

$`| z_n | > ER`$


```mermaid
graph TD
C{"Abs(z) > ER"}
C -->|Yes| D[Escaping ]
C -->|No| E[Non-escaping]
```

````c
 if fabs(z) > ER) return iColorOfExterior; // if escaping stop iteration
 // continue iteration
````


### attraction test

Checks if point is attracted to the finite attractor. In simplest case (cauliflower) attacting trap ( petal) for forward iteration 
is a circle ( gray):
* inside filled Julia set
* with parabolic fixed point on it's boundary

If point of forward orbit is inside such circle then it is attracted to the finite attractor.  

How to distinguish orbits? Using:
* x of z when point is inside
* y
* distance to the fixed point
* angle


See also images below:
* black dots = critical orbit
* gray circle = attracting petal = trap
* green dots = orbits
* red dot = first point of the orbit inside trap



![x.png](./images/x.png)  
![y.png](./images/y.png)  
![distance.png](./images/distance.png)  
![angle.png](./images/angle.png)  


## extended test
checks 2 things:
* if points escapes ( bailout test as above)
* if it is attracted to finite periodic point

```c
for (n=0; n < nMax; n++){
	if (IsInAttractingPetal(z)) return GiveColorOfInterior(z); 
	if (cabs2(z)>ER2) return iColorOfExterior;
   	z = z*z +c ; /* forward iteration : complex quadratic polynomial */
   	}
printf("unknown point ( periodic ? or slow dynamics ?) : z = %.16f; %.16f\n", creal(z), cimag(z));
NoOfUnknownPoints +=1;
return iColorOfUnknown;
```




```mermaid
graph TD
C{"escaping or attracting"}
C -->|escaping| D[Exterior ]
C -->|attracting| E[interior]
C -->|unknown|F[unknown]
```



# Files: 
* source code
  * c files : Console programs in c programming language with comments in english ( c extension)
    * [c.c](./src/c.c) = new algorithm for period 1 
    * [d.c](./src/d.c) = old algorithm for period 1  
  * Maxima CAS files ()
* README.md = text file with description in markdown format ( md extension)
  * [ta.mac](./src/ta.mac) file for creating images



# License

This project is licensed under the   Apache License Version 2.0, January 2004 - see the [LICENSE](LICENSE) file for details


# git

## commands
```
cd existing_folder
  git init
  git remote add origin git@gitlab.com:adammajewski/SepalsOfCauliflower.git
  git add .
  git commit
  git push -u origin master
```


```
  git clone git@gitlab.com:adammajewski/SepalsOfCauliflower.git
```

Subdirectory

```git
mkdir images
git add *.png
git mv  *.png ./images
git commit -m "move"
git push -u origin master
```
then link the images:

```txt
![](./images/n.png "description") 

```

```git
gitm mv -f 
```

to overwrite 




## local repo

```
~/c/julia/parabolic/1over1/sepals
```



# markdown format
[GitLab](https://gitlab.com/) uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)



